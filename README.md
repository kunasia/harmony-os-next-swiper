### 如果开源对你有帮助，也很希望得到你的鼓励，右上角Star🌟，等你点亮！

  ------------------------------------------------------------------


### 交流+V：lucy2188687859
<img src="entry/md_img/md_weixin_img.png" alt="My Image" width="200" height="200"/>

  ------------------------------------------------------------------

# HarmonyOS NEXT轮播图组件
## ✍介绍
采用HarmonyOS NEXT开发的图片轮播图组件，包含图片轮播、自定义角标，适用于广告轮播图片场景。
### 文件路径：entry/src/main/ets/pages/Index.ets
### 软件架构：
1.开发语言：Api12+Stage模式+ArkTS+ArkUI+Swiper
2.开发工具：DevEco Studio
3.项目运行：项目在开发工具中启动，效果可Preview 预览或模拟器或真机调试，具体操作可查看此文章https://blog.csdn.net/weixin_71403100/article/details/136150011?spm=1001.2014.3001.5502
### 效果预览
<img src="entry/md_img/md_swiper_img1.png" alt="My Image" width="300" height="500"/>
<img src="entry/md_img/md_swiper_img2.png" alt="My Image" width="300" height="500"/>

